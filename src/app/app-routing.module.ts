import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NewBillComponent } from './features/new-bill/new-bill.component';
import { TransactionHistoryComponent } from './dashboard/transaction-history/transaction-history.component';
import { LoginComponent } from './user/login/login.component';
import { AuthGuardGuard } from './auth-guard.guard';
import { AuthGuardService } from './auth/auth-guard.service';
import { AboutComponent } from './features/about/about.component';
import { MerchantPageComponent } from './features/merchant-page/merchant-page.component';
import { RoleGuard } from './role.guard';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'home', component: DashboardComponent, canActivate: [AuthGuardGuard]},
  {path: 'create-new', component: NewBillComponent, canActivate: [AuthGuardGuard]},
  {path: 'history', component: TransactionHistoryComponent, canActivate: [AuthGuardGuard]},
  {path: 'about', component: AboutComponent, canActivate: [AuthGuardGuard]},
  {path: 'merchant', component: MerchantPageComponent, canActivate: [RoleGuard]}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
