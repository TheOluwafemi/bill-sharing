export interface Bill {
    id?: string;
    initiator?: string;
    billName?: string;
    merchantID?: string;
    totalAmount?: number;
    friendsIDS?: string[];
    sharedAmount?: number;
    paid?: boolean;
}
