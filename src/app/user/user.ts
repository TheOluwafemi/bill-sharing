export class User {
    id?: string;
    username?: string;
    userType?: string;
    walletBallance?: number;
}
