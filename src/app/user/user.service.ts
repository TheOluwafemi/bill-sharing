import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from './user';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  allUsers: Observable<User[]>;
  userCollection: AngularFirestoreCollection<User[]>;
  userDocument: AngularFirestoreDocument<User>;
  userDetails: Observable<User>;

  constructor(private firestore: AngularFirestore,
    private authService: AuthService) { }

  getLoggedInUserDetails() {
    let uuid;
    this.authService.signInUser.subscribe(signedInUser => {
      // console.log('the signed in user', signedInUser);
      uuid = signedInUser.uid;
    });
    this.userDocument = this.firestore.collection('users').doc(uuid);
    this.userDetails = this.userDocument.valueChanges();
    console.log('the userdetails', this.userDetails);
  }

  getAllUsers() {
    this.userCollection = this.firestore.collection('users');
    this.allUsers = this.userCollection.snapshotChanges().pipe(
      map(changes => changes.map(a => {
        const data = a.payload.doc.data() as User;
        data.id = a.payload.doc.id;
        return data;
      }))
    );
  }
}
