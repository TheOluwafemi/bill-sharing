import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth/auth.service';
import { UserService } from './user/user.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {
  user: any;

  constructor(private _authService: AuthService, private _router: Router, private userService: UserService) {
  }


  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    this.userService.userDetails.subscribe(signedInUser => {
      console.log(signedInUser);
      this.user = signedInUser;
      console.log('user is here', this.user);
    });

    if (this.user['userType'] === 'merchant') {
      return true;
    }

    // navigate to not found page
    this._router.navigate(['/home']);
    return false;
  }

}



