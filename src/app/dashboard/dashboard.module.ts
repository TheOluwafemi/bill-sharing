import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { StatCardsComponent } from './stat-cards/stat-cards.component';
import { TransactionHistoryComponent } from './transaction-history/transaction-history.component';
import { PendingBillsComponent } from './pending-bills/pending-bills.component';
import { CreateNewBillComponent } from './create-new-bill/create-new-bill.component';
import { ReportNewIssueComponent } from './report-new-issue/report-new-issue.component';
import { CoreModule } from '../core/core.module';

@NgModule({
  declarations: [
    DashboardComponent, StatCardsComponent,
    TransactionHistoryComponent, PendingBillsComponent,
    CreateNewBillComponent, ReportNewIssueComponent
  ],
  imports: [
    CommonModule,
    CoreModule
  ],
  exports: [
    DashboardComponent, StatCardsComponent,
    TransactionHistoryComponent, PendingBillsComponent,
    CreateNewBillComponent, ReportNewIssueComponent
  ]
})
export class DashboardModule { }
