import { Component, OnInit } from '@angular/core';
import { PendingBillsService } from './pending-bills.service';
import { Bill } from 'src/app/bill';

@Component({
  selector: 'app-pending-bills',
  templateUrl: './pending-bills.component.html',
  styleUrls: ['./pending-bills.component.scss']
})
export class PendingBillsComponent implements OnInit {
  pendingBills: Bill[];
  totalAmount: any;
  initiator: string;
  merchant: string;


  constructor(private pendingBillService: PendingBillsService) { }

  ngOnInit() {
    this.pendingBillService.fetchPendingBills();
    this.pendingBillService.userPendingBill.subscribe(bills => {
      this.pendingBills = bills;
      console.log('bills in ts', bills);

      this.pendingBills = Object.values(bills);

    });
  }

  onPay(bill) {
    console.log(bill);
  }
}
