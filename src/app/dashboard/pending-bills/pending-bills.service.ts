import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Bill } from 'src/app/bill';
import { map } from 'rxjs/operators';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class PendingBillsService {
  allBills: Observable<Bill[]>;
  billsDocument: AngularFirestoreDocument<Bill[]>;
  userPendingBill: Subject<any> = new BehaviorSubject<any>([]);
  uuid: string;

  constructor(private firestore: AngularFirestore,
    private authService: AuthService) { }

  fetchPendingBills() {
    this.authService.signInUser.subscribe(signedInUser => {
      this.uuid = signedInUser.uid;
    });
    console.log('id', this.uuid);
    this.billsDocument = this.firestore.collection('bill').doc(this.uuid);
    this.allBills = this.billsDocument.valueChanges();

    this.getUserPendingBills();
  }

  getUserPendingBills() {
    this.allBills.subscribe(bill => {
      console.log('All bills in service', bill);

      this.userPendingBill.next(bill);
    });
  }
}
