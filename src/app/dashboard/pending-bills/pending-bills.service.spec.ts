import { TestBed } from '@angular/core/testing';

import { PendingBillsService } from './pending-bills.service';

describe('PendingBillsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PendingBillsService = TestBed.get(PendingBillsService);
    expect(service).toBeTruthy();
  });
});
