import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportNewIssueComponent } from './report-new-issue.component';

describe('ReportNewIssueComponent', () => {
  let component: ReportNewIssueComponent;
  let fixture: ComponentFixture<ReportNewIssueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportNewIssueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportNewIssueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
