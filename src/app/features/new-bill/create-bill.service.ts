import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Bill } from '../../bill';

@Injectable({
  providedIn: 'root'
})
export class CreateBillService {

  constructor(private firestore: AngularFirestore) { }

  createBill(bill: Bill) {
    return this.firestore.collection('bill').doc(bill.initiator).set({[bill.id]:bill}, {merge: true});
  }

}
