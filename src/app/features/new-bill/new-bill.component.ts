import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { CreateBillService } from './create-bill.service';
import { UserService } from 'src/app/user/user.service';
import { User } from 'src/app/user/user';
import { AuthService } from 'src/app/auth/auth.service';
import { Bill } from 'src/app/bill';
import { Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-new-bill',
  templateUrl: './new-bill.component.html',
  styleUrls: ['./new-bill.component.scss']
})
export class NewBillComponent implements OnInit {

  isLinear = false;
  billDetailsForm: FormGroup;
  splittingForm: FormGroup;
  createBillTestForm: FormGroup;
  numberOfFriends: number;
  individualBill: number;
  nameOfBill: any;

  constructor(private _formBuilder: FormBuilder,
    private createBill: CreateBillService,
    private userService: UserService,
    private authService: AuthService,
    private router: Router,
    public toastr: ToastrManager) { }

  userList: User[] = [];
  userDetails: User;

  totalAmount: number = 0;

  bill: Bill;

  ngOnInit() {

    this.authService.signInUser.subscribe(signedInUser => {
      this.userDetails = {
        id: signedInUser.uid,
      };
    });

    this.billDetailsForm = this._formBuilder.group({
      billName: ['', Validators.required],
      merchantID: ['', Validators.required]
    });
    this.splittingForm = this._formBuilder.group({
      splittingMethod: ['', Validators.required],
      totalAmount: ['', Validators.required],
      friends: [[''], Validators.required]
    });

    this.userService.getAllUsers();
    this.userService.allUsers.subscribe(users => {
     const  filteredUsers = users.filter(user => user.id !== this.userDetails.id && user.userType !== 'merchant');
      this.userList = filteredUsers;
    });

  }

  onSubmitBill(billDetailsForm: any, splittingForm: any) {
    console.log(billDetailsForm, splittingForm);

    const individualAmount = this.processBill(splittingForm.totalAmount, splittingForm.friends.length);

    this.bill = {
      id: billDetailsForm.merchantID.replace(/ /g, '').concat(new Date().toLocaleDateString()),
      initiator: this.userDetails.id,
      billName: billDetailsForm.billName,
      merchantID: billDetailsForm.merchantID,
      totalAmount: splittingForm.totalAmount,
      friendsIDS: splittingForm.friends,
      sharedAmount: individualAmount
    };

    console.log(this.bill);
    this.individualBill = null;
    this.numberOfFriends = null;

    this.createBill.createBill(this.bill);

    this.toastr.infoToastr('Bill created, payment will be made when your friends accept the bill');

    this.router.navigate(['/home']);
  }

  processBill(amount: number, numberOfIndividuals: number) {
    const individualAmount = amount / (numberOfIndividuals + 1);
    return individualAmount;
  }

  setAmount(form: any) {
    const individualAmount = form.totalAmount / (form.friends.length + 1);
    this.numberOfFriends = form.friends.length + 1;
    this.individualBill = individualAmount;
    this.nameOfBill = form.billName;
  }

}

// export interface Bill {
//   id?: string;
//   initiator: string;
//   billName: string;
//   merchantID: string;
//   totalAmount: number;
//   friendsIDS: string[];
//   sharedAmount: number;
// }


