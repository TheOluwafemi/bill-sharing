import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeaturesComponent } from './features.component';
import { NewBillComponent } from './new-bill/new-bill.component';
import { MaterialModule } from '../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AboutComponent } from './about/about.component';
import { TransactionHistoryPageComponent } from './transaction-history-page/transaction-history-page.component';
import { CoreModule } from '../core/core.module';
import { MerchantPageComponent } from './merchant-page/merchant-page.component';

@NgModule({
  declarations: [FeaturesComponent, NewBillComponent, AboutComponent, TransactionHistoryPageComponent, MerchantPageComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    AngularFirestoreModule,
    CoreModule
  ],
  exports: [
    NewBillComponent,
    FeaturesComponent,
    AboutComponent, 
    TransactionHistoryPageComponent, 
    MerchantPageComponent
  ]
})
export class FeaturesModule { }
