import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth/auth.service';
import { UserService } from './user/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Bill';
  user: any;

  constructor(private authService: AuthService, private userService: UserService) { }

  ngOnInit() {
    // console.log(this.authService.isLoggedIn());
    // console.log(this.userService.getLoggedInUserDetails());

    this.userService.userDetails.subscribe(signedInUser => {
      console.log(signedInUser);
      // this.user = signedInUser;
    });

  }
}
