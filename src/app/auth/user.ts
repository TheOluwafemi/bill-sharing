export class User {
    id?: string;
    username?: string;
    walletBallance?: string;
    userType?: string;
}
