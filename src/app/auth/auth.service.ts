import { Injectable, NgZone } from '@angular/core';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { auth } from 'firebase/app';
// import { User } from 'firebase';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { User } from './user';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userData: any;
  signInUser: Subject<any> = new BehaviorSubject<any>('');

  constructor(
    public  afAuth:  AngularFireAuth,
    public  router:  Router,
    public afs: AngularFirestore,   // Inject Firestore service
    public ngZone: NgZone // NgZone service to remove outside scope warning

    ) {
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userData = user;
        console.log('Logged in user:', this.userData);
        this.signInUser.next(user);
        localStorage.setItem('user', JSON.stringify(this.userData));
        JSON.parse(localStorage.getItem('user'));
      } else {
        localStorage.setItem('user', null);
        JSON.parse(localStorage.getItem('user'));
      }
    });
  }

  // Sign in with email/password
  SignIn(email, password) {
    console.log('got here');
    return this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then((result) => {
        console.log('got here');
        this.ngZone.run(() => {
          console.log('routing to home');
          this.router.navigate(['/home']);
          console.log('routed');
        });
        this.SetUserData(result.user);
      }).catch((error) => {
        window.alert(error.message);
        console.log(this.isLoggedIn());
      });
  }

  /* Setting up user data when sign in with username/password,
  sign up with username/password and sign in with social auth
  provider in Firestore database using AngularFirestore + AngularFirestoreDocument service */
  SetUserData(user) {
    console.log('user here', user);
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const userData: User = {
      id: user.uid,
      username: user.username
      // userType: user.userType
    };
    return userRef.set(userData, {
      merge: true
    });
  }


  // Returns true when user is looged in and email is verified
  isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return (user !== null) ? true : false;
  }


  // Sign out
  SignOut() {
    return this.afAuth.auth.signOut().then(() => {
      localStorage.removeItem('user');
      this.router.navigate(['login']);
    });
  }

}
