import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { UserService } from 'src/app/user/user.service';
import { User } from 'src/app/user/user';

@Component({
  selector: 'app-main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.scss']
})
export class MainHeaderComponent implements OnInit {

  title = 'monami';
  userDetails: User = {
    id: '',
    username: '',
    walletBallance: 0
  };

  constructor(private authService: AuthService,
    private userService: UserService) { }

  ngOnInit() {

    console.log(this.authService.isLoggedIn());

    setTimeout(() => {

      this.userService.getLoggedInUserDetails();
      this.userService.userDetails.subscribe(signedInUser => {
        console.log(signedInUser);
        this.userDetails = {
          username: signedInUser.username,
          walletBallance: signedInUser.walletBallance
        };
      });

      console.log(this.userDetails);

    }, 5000);

  }

}
