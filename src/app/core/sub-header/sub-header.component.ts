import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { RoleGuard } from 'src/app/role.guard';
import { UserService } from 'src/app/user/user.service';

@Component({
  selector: 'app-sub-header',
  templateUrl: './sub-header.component.html',
  styleUrls: ['./sub-header.component.scss']
})
export class SubHeaderComponent implements OnInit {
  user: any;
  authorised: boolean;

  constructor(private router: Router, private authService: AuthService, private roleGuard: RoleGuard, private userService: UserService) { }

  ngOnInit() {
    this.checkRole();
  }

  checkRole (){
    this.userService.userDetails.subscribe(signedInUser => {
      this.user = signedInUser;
    });

    if (this.user['userType'] === 'merchant') {
      this.authorised = true;
    }
  }

}
